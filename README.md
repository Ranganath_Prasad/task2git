### How do I get set up? ###
1)Git clone the code to a folder(Ex: task2)

2)Go inside the folder task2 and open task2git folder(In terminal) 

3)Node js and express should be installed else please install both 

4)Install npm cors,request,cheerio,querystring,jcookie modules 

5)run the program that is "node bin/www" 

6)Open browser and enter url "http://localhost:3000/api/keyword" 

ex: "http://localhost:3000/api/tv" It will return the total number of results for the search and number of pages which has this results.

 7)open browser and enter url "http://localhost:3000/api/pagenumber/keyword"

 ex: "http://localhost:3000/api/2/tv" It will return all the number of results on the page number 5 for the keyword tv, If the page number is not present then it will display wether keyword is there are not, if it's there it will return number of results for keyword and Number of pages containing the result.