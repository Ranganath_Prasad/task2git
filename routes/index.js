var express = require('express');
var router = express.Router();
var cors = require('cors');
var request = require('request');
var cheerio = require('cheerio');
var querystring = require('querystring');
var j = request.jar();
var cookie = request.cookie('');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/api/:id', cors(),function(req, res) {
	initQuery1(req,res);
});

router.get('/api/:pageNumber/:id', cors(),function(req, res) {
  initQuery2(req,res);
});

function initQuery1(req, res){
  var id = req.param('id',null);
  id = id.trim();
  id = decodeURIComponent(id);
  var url = 'http://www.shopping.com/';
  j.setCookie(cookie, url);
  var cookie_string;
  request({url: url, jar: j}, function (error, response, body) {
    cookie_string = j.getCookieString(url); 
    if(cookie_string != undefined){
      cookie_string = 'remove'+cookie_string;
      cookie_string = cookie_string.split('remove; ')[1];
      // console.log('cookie_string is '+cookie_string);
      var data = querystring.stringify({
    'CLT':'SCH',
    'KW':id
  });
  console.log('url is '+'http://www.shopping.com/'+id+'/products?CLT=SCH&KW='+id+'')
  request({
    url:'http://www.shopping.com/'+id+'/products?CLT=SCH&KW='+id+'',
    gzip: true,
    method: "GET",
    headers: {
      'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
      'Accept-Encoding':'gzip, deflate, sdch',
      'Accept-Language':'en-US,en;q=0.8',
      'Cache-Control':'no-cache',
      'Connection':'keep-alive',
      'Content-Length':Buffer.byteLength(data),
      'Cookie':cookie_string,
      'Host':'www.shopping.com',
      'Pragma':'no-cache',
      'Referer':'http://www.shopping.com/'+id+'/products?CLT=SCH&KW='+id+'',
      'Upgrade-Insecure-Requests':1,
      'User-Agent':'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/50.0.2661.102 Chrome/50.0.2661.102 Safari/537.36'
    },
    body: data
    },
    function(err, resp, body){
      if(resp != undefined){
      var $ = cheerio.load(body);
      var $ = cheerio.load(body);
      var checkForAnyResult = $('.rtCol').text();
      if(checkForAnyResult){
      checkForAnyResult = checkForAnyResult.trim();
      res.send('Sorry, There were no matches for your search')
      }else{
      var numberOfResults = $('.numTotalResults').text();
      numberOfResults = numberOfResults.split('of');
      var lastPage = $('.paginationNew').text();
      lastPage = lastPage.trim();
      lastPage = lastPage.split('Next');
      var n = lastPage[0];
      n = n.trim();
      n = n.replace(/\s/g,'');
      console.log('n is '+n)
      var d = [];
      d.push('Total number of results for your search '+id+' is  :'+''+numberOfResults[1]+'');
      d.push('Pages containing the searched keyword results are range from :'+''+n+'')
      res.send(JSON.stringify(d));
      // res.send('Total number of results for your search '+id+' is '+numberOfResults[1]+' Pages containing the searched keyword results are range from '+n);
      }
      }else{
      res.send('sorry, Please try again');
      }
     });
    }
  })
  
  // var url = 'http://www.shopping.com/'+id+'/products?CLT=SCH&KW='+id+'';
  // console.log('url is '+url)
  // request({url: url}, function (error, response, body) {
  //   if(response != undefined){
  //   	var $ = cheerio.load(body);
  //   	var checkForAnyResult = $('.rtCol').text();
  //   	if(checkForAnyResult){
  //   		checkForAnyResult = checkForAnyResult.trim();
  //   		res.send('Sorry, There were no matches for your search')
  //   	}else{
  //   		var numberOfResults = $('.numTotalResults').text();
	 //    	numberOfResults = numberOfResults.split('of');
  //       var lastPage = $('.paginationNew').text();
  //       lastPage = lastPage.trim();
  //       lastPage = lastPage.split('Next');
  //       var n = lastPage[0];
  //       n = n.trim();
	 //    	res.send('Total number of results for your search '+id+' is '+numberOfResults[1]+' Pages containing the searched keyword results are range from '+n);
  //   	}
  //   }else{
  //   	res.send('sorry, Please try again');
  //   }
  // })   
}

function initQuery2(req,res){
  var id = req.param('id',null);
  var pageNumber = req.param('pageNumber',null);
  id = id.trim();
  pageNumber = pageNumber.trim();
  id = decodeURIComponent(id);
  pageNumber = decodeURIComponent(pageNumber);
  var url = 'http://www.shopping.com/';
  j.setCookie(cookie, url);
  var cookie_string
  request({url: url, jar: j}, function (error, response, body) {
    cookie_string = j.getCookieString(url); 
    if(cookie_string != undefined){
      cookie_string = 'remove'+cookie_string;
      cookie_string = cookie_string.split('remove; ')[1];
      // console.log('cookie_string is '+cookie_string);
      var data = querystring.stringify({
    'KW':id
  }); 
  request({
    url:'http://www.shopping.com/'+id+'/products~PG-'+pageNumber+'?KW='+id+'',
    gzip: true,
    method: "GET",
    headers: {
      'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
      'Accept-Encoding':'gzip, deflate, sdch',
      'Accept-Language':'en-US,en;q=0.8',
      'Cache-Control':'no-cache',
      'Connection':'keep-alive',
      'Content-Length':Buffer.byteLength(data),
      'Cookie':cookie_string,
      'Host':'www.shopping.com',
      'Pragma':'no-cache',
      'Referer':'http://www.shopping.com/'+id+'/products?CLT=SCH&KW='+id+'',
      'Upgrade-Insecure-Requests':1,
      'User-Agent':'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/50.0.2661.102 Chrome/50.0.2661.102 Safari/537.36'
    },
    body: data
    },
    function(err, resp, body){
      var $ = cheerio.load(body);
      var checkError = $('.contentErrMsg').text();
      var checkNomatch = $('.rtCol').text();
      if(checkError){
        initQuery1(req,res);
      }else if(checkNomatch){
        initQuery1(req,res);
      }else{
        var resultsAre = $('.productName').text();
        resultsAre = resultsAre.split('...');
        var d = [];
        for (var i=0;i<resultsAre.length-1;i++){
          d.push('productName :'+''+resultsAre[i].trim()+'');
          // console.log('results is '+resultsAre[i].trim());
        }
        res.send(JSON.stringify(d));
      }
     });
    }
  })
}

module.exports = router;
